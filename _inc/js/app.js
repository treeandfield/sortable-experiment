var myapp = angular.module('sortableApp', []);


myapp.controller('sortableController', function ($scope, $timeout, $compile) {
  
  
  $scope.field = [];
  
  var display = function(){
	  $scope.display = JSON.stringify($scope.rootItem, null,"    ");
  }
  
  for(i=0; i<100; i++){
	    $scope.field.push({label:"item" + i, field:true});
  }
  
  $scope.tree = {
	  content:{
		   tree:[
			   {
				 label: 'Item',
				 tree:true,
				 toggle:false,
				 content:{
				 	tree:[]
				 }
			   }
		   ]
	    }
  }
 
  $scope.add = function(){
	   
	   var obj = {
				 label: 'Item' + $scope.tree.content.tree.length,
				 tree:true,
				 toggle:false,
				 content:{
				 	tree:[]
				 }
		};
			   	   
	   $scope.tree.content.tree.unshift(obj);
	   display();
  }
  
  for(i=0; i<5; i++){
	  $scope.add(); 
  }

  // --- 
  /*
    $timeout(function () { 
        
         var t =  $('.tree');
         var sort = Sortable.create(t[0], {
		 	animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
		 	onUpdate: function (evt){
		 		//console.log($('.tree').html());
		 		
		 		  // $compile($('.tree').html())($scope);
		 		  // $scope.$digest();
		 	}
		 });
	    
    }, 100, false);
  */
  
  display();
  

  
});